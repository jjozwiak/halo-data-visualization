weapons_meta = [
	{
		"Weapon": "DMR - M392",
		"Type": "Mid Range"
	},
	{
		"Weapon": "Melee",
		"Type": "Short Range"
	},
	{
		"Weapon": "Magnum - M6G Pistol",
		"Type": "Mid Range"
	},
	{
		"Weapon": "Frag Grenade - M9 HE-DP",
		"Type": "Short Range"
	},
	{
		"Weapon": "Gravity Hammer - T2 EW/H",
		"Type": "Short Range"
	},
	{
		"Weapon": "Needler Rifle - T31 Rifle",
		"Type": "Long Range"
	},
	{
		"Weapon": "Sniper Rifle - SRS99",
		"Type": "Long Range"
	},
	{
		"Weapon": "Energy Sword - T1 EW/S",
		"Type": "Short Range"
	},
	{
		"Weapon": "Assault Rifle - MA37 ICWS",
		"Type": "Short Range"
	},
	{
		"Weapon": "Needler - T33 GML",
		"Type": "Mid Range"
	},
	{
		"Weapon": "Shotgun - M45 TS",
		"Type": "Short Range"
	},
	{
		"Weapon": "Plasma Grenade - T1 AP-G",
		"Type": "Mid Range"
	},
	{
		"Weapon": "Wraith - Type 26 AGC (Driver)",
		"Type": "Vehicle"
	},
	{
		"Weapon": "Banshee Bomb - T26 GSA",
		"Type": "Vehicle"
	},
	{
		"Weapon": "Plasma Repeater - T51 DER/I",
		"Type": "Short Range"
	},
	{
		"Weapon": "Scorpion - M808B MBT",
		"Type": "Vehicle"
	},
	{
		"Weapon": "Ghost - T32 RAV",
		"Type": "Vehicle"
	},
	{
		"Weapon": "Concussion Rifle - T50 DER/H",
		"Type": "Mid Range"
	},
	{
		"Weapon": "Bomb - No. 14 Anti-tank Mine",
		"Type": "Vehicle"
	},
	{
		"Weapon": "Rocket Launcher - M41SSR",
		"Type": "Mid Range"
	},
	{
		"Weapon": "Warthog - M41 LAAG",
		"Type": "Vehicle"
	},
	{
		"Weapon": "GrenadeLauncher - M319 IGL",
		"Type": "Short Range"
	}
];