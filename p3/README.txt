CS171 P3
Halo Reach: Investigating Playstyles

AUTHORS
Sebastian Pierce-Durance
Jordan Jozwiak

FILE STRUCTURE
index.html - Halo Reach: Investigating Playstyles visualization
css/
    styles_bar.css
    styles_bubble.css
    styles_cal.css
    styles_combined.css
    styles_player_info.css
    styles_scatter.css

data/
	scatter_data.js - used for the scatter plot, bar chart, and player info
	medals_meta.js - used for bubble chart
	weapons_meta.js
	calendar_data.js - used for the calendar

js/
	Javascript files named for corresponding visualizations
	External library files for D3, JQuery, and Intro.js


REFERENCES
We ended up using the intro.js library to help guide our users through their 
first experience of our visualization.  The documentation can be found here:
http://usablica.github.io/intro.js/ 
and the code was downloaded from here: 
https://github.com/usablica/intro.js/tree/v0.3.0.

Basic code for calendar: http://bl.ocks.org/mbostock/4063318
Basic code for scatterplot: http://alignedleft.com/tutorials/d3/making-a-scatterplot/
Basic code for barchart: http://bl.ocks.org/mbostock/3885304 
Basic code for bubble chart: http://bl.ocks.org/mbostock/3887235
Basic code for pie charts: http://bl.ocks.org/mbostock/4063269
Negative value barchart guide: http://bl.ocks.org/mbostock/2368837
Chart transition guide: http://mbostock.github.io/d3/tutorial/bar-2.html
